<?php


namespace App\Repositories;


use App\Models\Recipe;

class RecipeRepository
{
    public function create($uuid,$medicine, $unit_of_measurement, $dose, $frequency,$route_of_administration,$addtional_indications, $start_date,
    $ending_date,$inquiries_id)
    {
        $data['uuid'] = $uuid;
        $data['medicine'] = $medicine;
        $data['unit_of_measurement'] = $unit_of_measurement;
        $data['dose'] = $dose;
        $data['frequency'] = $frequency;
        $data['route_of_administration'] = $route_of_administration;
        $data['addtional_indications'] = $addtional_indications;
        $data['start_date'] = $start_date;
        $data['ending_date'] = $ending_date;
        $data['inquiries_id'] = $inquiries_id;

        return recipe::create($data);


    }

    public function updated($uuid,$medicine, $unit_of_measurement, $dose, $frequency,$route_of_administration,$addtional_indications, $start_date,
    $ending_date,$inquiries_id){
        $recipe = $this->find($uuid);
        $recipe->medicine = $medicine;
        $recipe->unit_of_measurement = $unit_of_measurement;
        $recipe->dose = $dose;
        $recipe->frequency = $frequency;
        $recipe->route_of_administration = $route_of_administration;
        $recipe->addtional_indications = $addtional_indications;
        $recipe->start_date = $start_date;
        $recipe->ending_date = $ending_date;
        $recipe->inquiries_id = $inquiries_id;

        return $recipe->save();

    }

    public function delete($uuid){
        $recipe = $this->find($uuid);
        return $recipe->delete();
    }
    public function find($uuid){
        return Recipe::where('uuid', '=', $uuid)->first();
    }
    public function list(){
        return Recipe::all();
    }
}
