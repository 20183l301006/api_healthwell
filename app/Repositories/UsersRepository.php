<?php


namespace App\Repositories;


// use App\Persons;
use App\Models\User;

class UsersRepository
{
    public function create($uuid,$name,$ap_patern,$ap_matern,$email,$password,$validation,$persons_id){
        $user['uuid'] = $uuid;
        $user['name'] = $name.' '.$ap_patern.' '.$ap_matern;
        $user['email'] = $email;
        $user['password'] = $password;
        $user['validation'] = $validation;
        $user['persons_id'] = $persons_id;

        return User::create($user);
    }
        public function update($uuid,$name,$ap_patern,$ap_matern,$email)
        {
            $user = $this->find($uuid);
            $user->name = $name.' '.$ap_patern.' '.$ap_matern;
            $user->email = $email;

            return $user->save();
        }

        Public function find($uuid)
    {
        return User::where('uuid','=', $uuid)->first();
    }

    }

