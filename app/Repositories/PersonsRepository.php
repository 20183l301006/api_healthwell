<?php

namespace App\Repositories;

use App\Models\Persons;
use App\Models\User;
use App\Models\Roles;

use DB;

class PersonsRepository
{

    public function create($uuid,$name,$ap_patern,$ap_matern,$curp,$domicile,$cell_phone,$telefone, $photo ,$roles_id)
    {
        $person['uuid'] = $uuid;
        $person['name'] = $name;
        $person['ap_patern'] = $ap_patern;
        $person['ap_matern'] = $ap_matern;
        $person['curp'] = $curp;
        $person['domicile'] = $domicile;
        $person['cell_phone'] = $cell_phone;
        $person['telefone'] = $telefone;
        $person['photo'] = $photo;
        $person['roles_id'] = $roles_id;

        return Persons::create($person);
    }

    public function update($uuid,$name,$ap_patern,$ap_matern,$curp,$domicile,$cell_phone,$telefone,$photo)
    {
        $person = $this->find($uuid);
        $person->name = $name;
        $person ->ap_patern = $ap_patern;
        $person->ap_matern = $ap_matern;
        $person->curp = $curp;
        $person->domicile = $domicile;
        $person->cell_phone = $cell_phone;
        $person->telefone = $telefone;
        $person->photo = $photo;
        $person->save();
        return $person->id;
    }
    Public function find($uuid)
    {
        return Persons::where('uuid','=', $uuid)->first();
    }

    public function list()
    {
        $persons = Persons::with('users','doctors')->get();
        return $persons->toArray();
    }
    /*public function borrados(){
    $person = Persona::onlyTrashed()->get();

    return $person->toArray();
    }
    */
  /*  public function restore($id){
       return Persons::withTrashed()->find($id)->restore();

    }
    public function retornar($id){
        return Persons::onlyTrashed()->find($id);

    }
*/

}
