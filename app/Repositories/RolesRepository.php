<?php


namespace App\Repositories;


use App\Models\Roles;

class RolesRepository
{

    public function list(){
        return Roles::all();
    }

}
