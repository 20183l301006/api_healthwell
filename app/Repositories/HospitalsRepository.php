<?php


namespace App\Repositories;

use Uuids;

use App\Models\Hospitals;

class HospitalsRepository
{
    public function create($uuid,$name, $direction, $photo){

        $hospitals['uuid'] = $uuid;
        $hospitals['name'] = $name;
        $hospitals['direction'] = $direction;
        $hospitals['photo'] = $photo;
        return Hospitals::create($hospitals);


    }

    public function updated($uuid, $name, $direction, $photo){
        $hospitals = $this->find($uuid);
        $hospitals->name = $name;
        $hospitals->direction = $direction;
        $hospitals->photo = $photo;
        return $hospitals->save();

    }

    public function delete($uuid){
        $hospitals = $this->find($uuid);
        return $hospitals->delete();
    }
    public function list(){
        return Hospitals::all();
    }
    public function find($uuid){
        return Hospitals::where('uuid', '=', $uuid)->first();
    }

}
