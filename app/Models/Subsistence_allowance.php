<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class Subsistence_allowance extends Eloquent
{
    use SoftDeletes;
    protected $connection ="mongodb";
    protected $collection = 'subsistence_allowance';


    protected $fillable = [
        'allowed_foods', 'forbidden_foods', 'observer'
    ];
    protected $hidden = [
        'created_at','updated_at','deleted_at'
    ];
}
