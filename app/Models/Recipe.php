<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class Recipe extends Eloquent
{
    use SoftDeletes;
    protected $connection ="mongodb";
    protected $collection = 'recipe';


    protected $fillable = [
        'uuid','medicine', 'unit_of_measurement', 'dose', 'frequency', 'route_of_administration',
        'addtional_indications', 'start_date', 'ending_date', 'inquiries_id'
    ];
    protected $hidden = [
        'created_at','updated_at','deleted_at'
    ];
    public function persons(){
        return $this->belongsTo(Persons::class);
    }

    public function doctors(){
        return $this->belongsTo(Doctors::class);
    }
}
