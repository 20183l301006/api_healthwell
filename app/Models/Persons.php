<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class Persons extends Eloquent
{
    use SoftDeletes;
    protected $connection ="mongodb";
    protected $collection = 'persons';
    const DOCTORS = 2;
    const PATIENTS = 3;
    const DIRECTION_PATIENTS = " ";

    protected $primaryKey = '_id';
    protected $fillable = [
       '_id','uuid', 'name','ap_patern', 'ap_matern', 'curp', 'domicile', 'cell_phone', 'telefone', 'photo','roles_id'
    ];
    protected $hidden = [
        'created_at','updated_at'
    ];
    public function roles(){
        return $this->belongsTo(Roles::class);
    }
    public function users()
    {
        return $this->hasOne(User::class);
    }
    public function patients()
    {
        return $this->hasOne(Patients::class);
    }
    public function doctors()
    {
        return $this->hasOne(Doctors::class);
    }
    public function schedule_appointment()
    {
        return $this->hasOne(Schedule_appointment::class);
    }
    public function inquiries()
    {
        return $this->belongsToMany(inquiries::class);
    }

}
