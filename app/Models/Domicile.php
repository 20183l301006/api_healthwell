<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;


class Domicile extends Eloquent
{
    use SoftDeletes;
    protected $connection ="mongodb";
    protected $collection = 'domicile';


    protected $fillable = [
        'type', 'street', 'number_ext', 'number_int', 'state', 'municipality', 'location', 'colony', 'postalCode'
    ];
    protected $hidden = [
        'created_at','updated_at','deleted_at'
    ];
}
