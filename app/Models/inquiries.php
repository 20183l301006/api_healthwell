<?php

namespace App\Models;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class inquiries extends Model
{
    use SoftDeletes;
    protected $connection ="mongodb";
    protected $collection = 'inquiries';

    const TRATAMIENTO = True;

    protected $fillable = [
       '_id','uuid','num_inquirie','tratamiento','patients_id','doctors_id'
    ];
    protected $hidden = [
        'created_at','updated_at','deleted_at'
    ];

    public function patients(){
        return $this->belongsTo(Patients::class,'patients_id','_id');
    }
    public function doctors(){
        return $this->belongsTo(Doctors::class,'doctors_id','_id');
    }
    public function persons()
    {
        return $this->belongsToMany(Persons::class);

    }
}
