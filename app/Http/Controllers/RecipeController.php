<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\RecipeRepository;
use App\Models\Recipe;
use DB;
use Uuid;

class RecipeController extends Controller
{
    protected $recipe_respository;

    public function __construct(RecipeRepository $repository){
        $this->recipe_respository = $repository;
    }

    public function create(Request $request){

        $uuid = Uuid::generate()->string;
        $medicine = $request->input('medicine');
        $unit_of_measurement =$request->input('unit_of_measurement');
        $dose =$request->input('dose');
        $frequency =$request->input('frequency');
        $route_of_administration =$request->input('route_of_administration');
        $addtional_indications =$request->input('addtional_indications');
        $start_date =$request->input('start_date');
        $ending_date =$request->input('ending_date');
        $inquiries_id =$request->input('inquiries_id');
        return response()->json($this->recipe_respository->create($uuid,$medicine,$unit_of_measurement,$dose,$frequency,
        $route_of_administration,$addtional_indications,$start_date,$ending_date,$inquiries_id));
    }

    /////////////////////////////////////////////
    public function updated(Request $request, $uuid)
    {
        $medicine = $request->input('medicine');
        $unit_of_measurement =$request->input('unit_of_measurement');
        $dose =$request->input('dose');
        $frequency =$request->input('frequency');
        $route_of_administration =$request->input('route_of_administration');
        $addtional_indications =$request->input('addtional_indications');
        $start_date =$request->input('start_date');
        $ending_date =$request->input('ending_date');
        $inquiries_id =$request->input('inquiries_id');
       return response()->json($this->recipe_respository->updated($uuid,$medicine,$unit_of_measurement,$dose,$frequency,
       $route_of_administration,$addtional_indications,$start_date,$ending_date,$inquiries_id));
    }


    public function delete($uuid){
        return response()->json($this->recipe_respository->delete($uuid));
    }
    public function list(){
        return response()->json($this->recipe_respository->list());
    }


   /* public function search(Request $request){
       $search = $request->input('search');
    return response()->json($this->recipe_respository->search($search));
    }*/


    public function editar($uuid)
    {
        return response()->json($this->recipe_respository->find($uuid));
    }

}
