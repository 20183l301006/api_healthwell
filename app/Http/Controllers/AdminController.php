<?php
namespace App\Http\Controllers;
use App\Repositories\PersonsRepository;
use App\Repositories\UsersRepository;

use App\Models\User;
use App\Models\Persons;
use App\Models\Roles;

use DB;
// use http\Env\Response;
use Illuminate\Http\Request;

use JWTAuth;
use Mail;
use File;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Uuid;
use Illuminate\Http\Response;
class AdminController extends Controller
{
    protected $users_repository;
    protected $persons_repository;


    public function __construct(UsersRepository $_users, PersonsRepository $_persons)
    {
    $this->users_repository = $_users;
    $this->persons_repository = $_persons;



    }

    public function register(Request $request)
    {

    $validator = Validator::make($request->all(), [
    'name' => 'required|string|max:30',
    'ap_patern' => 'required|string|max:30',
    'ap_matern' => 'required|string|max:30',
    'curp' => 'required|string|max:18',

    ]);
    if($validator->fails()){
    return response()->json($validator->errors()->toJson(), 400);
    }
    if ($request->isMethod('post')) {
        try{
        if ($request->file('photo') != null) {
        $mime = "";
        try {
        $mime = $request->file('photo')->getMimeType();
        } catch (\Exception $ex) {
        return response()->json('No existe la imagen');
        }
        $name = strtolower($request->file('photo')->getClientOriginalName());
        $name = str_replace(' ', '_', $name);
        $path = public_path('images');
        $request->file('imagen')->move($path, $name);
        } else {
        $name = 'default.jpg';
        }
        }catch (\Exception $ext) {
        return response()->json('No existe la imagen');
        }
        }



      $person = $this->persons_repository->create(Uuid::generate()->string,$request->get('name'),$request->get('ap_patern'),
        $request->get('ap_matern'),$request->get('curp'),Persons::DIRECTION_PATIENTS,$request->get('cell_phone'),
        $request->get('telefone'),$name,$request->get('roles_id'));


    $user = $this->users_repository->create(Uuid::generate()->string,$request->get('name'),$request->get('ap_patern'),
    $request->get('ap_matern'),$request->get('email'),Hash::make($request->get('password')),
    $request->get('validation').substr($request->get('name'), 0, 3) .substr($request->get('email'), 0, 3).'2020',
    $person->id);



    $token = JWTAuth::fromUser($user);

    //0una vez Realizado el registro se envia un correo de confirmacion
    $datas['subject'] = 'HealthWell';
    $datas['for'] = $user['email'];
    Mail::send('mail.mail',['user' =>$user], function($msj) use($datas){
    $msj->from("healthwellapp@gmail.com","HealthWell");
    $msj->subject($datas['subject']);
    $msj->to($datas['for']);
    });
    return response()->json(compact('user','token','person'),201);
    }


    public function delete($uuid)
    {
       $user = User::where('uuid','=',$uuid)->first();
        $user->persons->delete();
        $user->delete();

    return response()->json('Datos eliminados');

    }

    public function updated(Request $request, $uuid)
    {
        $user2 = User::where('uuid','=',$uuid)->first();

    $person = $this->persons_repository->update($user2->persons->uuid,$request->get('name'),$request->get('ap_patern'),
    $request->get('ap_matern'),$request->get('curp'),$request->get('domicile'),$request->get('cell_phone'),
    $request->get('telefone'),$request->get('photo'));

    $user = $this->users_repository->update($user2->uuid,$request->get('name'),$request->get('ap_patern'),
    $request->get('ap_matern'),$request->get('email'));




    return response()->json(compact('user','person'),201);

    }


    public function editar($uuid)
    {
    //
    $user = User::where('uuid','=',$uuid)->first();
    $person = Persons::where('uuid','=',$user->persons->uuid)->first();


    $masvar = [
    'id'=>$person['id'],
    'uuid'=>$user['uuid'],
    'name'=>$person['name'],
    'ap_patern'=>$person['ap_patern'],
    'ap_matern'=>$person['ap_matern'],
    'curp'=>$person['curp'],
    'domicile'=>$person['domicile'],
    'cell_phone'=>$person['cell_phone'],
    'telefone'=>$person['telefone'],
    'photo'=>$person['photo'],
    'roles_id'=>$person['roles_id'],
    'email'=>$user['email'],
    'name'=>$user['name'],
    'persons_id'=>$user['persons_id']

    ];

    return response()->json($masvar);
    }

    public function return_image($name)
    {
    $imagen = \Storage::disk('images')->exists($name);

    if($imagen){
    $file = \Storage::disk('images')->get($name);
    return new Response ($file,201);
    }else{
     return response()->json('No existe la imagen');

    }

    }

    public function upload(Request $request){
    $image = $request->file('file0');



    $validator = Validator::make($request->all(), [

    'file0' => 'mimes:jpeg,jpg,png|required',

    ]);
    if($validator->fails()){
    return response()->json($validator->errors()->toJson(), 400);
    }


    $image_name = time().$image->getClientOriginalName();
    \Storage::disk('images')->put($image_name, \File::get($image));
    $data = array(
        'code' => 200,
        'imagen' => $image_name,
        'status' => 'success'
    );

        return response()->json($data, $data['code']);
    }
    }

