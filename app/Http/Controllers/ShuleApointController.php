<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ScheduleAppointmentRepository;
use App\Models\Schedule_appointment;
use DB;
use Uuid;

class ShuleApointController extends Controller
{
    protected $shedule_appointment_respository;

    public function __construct(ScheduleAppointmentRepository $repository){
        $this->shedule_appointment_respository = $repository;
    }

    public function create(Request $request){
        $uuid = Uuid::generate()->string;
        $date = $request->input('date');
        $turn = $request->input('turn');
        $patients_id = $request->input('patients_id');
       return response()->json($this->shedule_appointment_respository->create($uuid,$date, $turn, $patients_id));
    }

    /////////////////////////////////////////////
    public function updated(Request $request,$uuid){
        $date = $request->input('date');
        $turn = $request->input('turn');
        $patients_id = $request->input('patients_id');
       return response()->json($this->shedule_appointment_respository->updated($uuid, $date, $turn, $patients_id));
    }
    public function delete($uuid){
        return response()->json($this->shedule_appointment_respository->delete($uuid));
    }
    public function list(){
        return response()->json($this->shedule_appointment_respository->list());
    }


    public function editar($uuid)
    {
        $otraVar = Schedule_appointment::where('uuid','=',$uuid)->first();
        $masvar = [
            'id'=>$otraVar['id'],
            'date'=>$otraVar['date'],
            'turn'=>$otraVar['turn'],
            'patients_id'=>$otraVar['patients_id'],
        ];
        return response()->json($masvar);
    }


}
