<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\PatientsRepository;
use App\Repositories\PersonsRepository;
use App\Repositories\UsersRepository;
use App\Models\Patients;
use App\Models\Hospitals;
use App\Models\User;
use App\Models\Persons;
use DB;
use JWTAuth;
use Mail;
use File;

use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Uuid;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class PatientsController extends Controller
{
    protected $patients_respository;
    protected $users_repository;
    protected $persons_repository;


    public function __construct(UsersRepository $_users, PersonsRepository $_persons, PatientsRepository $repository){
        $this->users_repository = $_users;
        $this->persons_repository = $_persons;
        $this->patients_respository = $repository;
    }

    public function create(Request $request){


    $validator = Validator::make($request->all(), [
    'name' => 'required|string|max:30',
    'ap_patern' => 'required|string|max:30',
    'ap_matern' => 'required|string|max:30',
    'curp' => 'required|string|max:18',

    ]);
    if($validator->fails()){
    return response()->json($validator->errors()->toJson(), 400);
    }
    if ($request->isMethod('post')) {
        try{
        if ($request->file('photo') != null) {
        $mime = "";
        try {
        $mime = $request->file('photo')->getMimeType();
        } catch (\Exception $ex) {
        return response()->json('No existe la imagen');
        }
        $name = strtolower($request->file('photo')->getClientOriginalName());
        $name = str_replace(' ', '_', $name);
        $path = public_path('images');
        $request->file('imagen')->move($path, $name);
        } else {
        $name = 'default.jpg';
        }
        }catch (\Exception $ext) {
        return response()->json('No existe la imagen');
        }
    }

    $person = $this->persons_repository->create(Uuid::generate()->string,$request->get('name'),$request->get('ap_patern'),
    $request->get('ap_matern'),$request->get('curp'),Persons::DIRECTION_PATIENTS,$request->get('cell_phone'),
    $request->get('telefone'),$name,Persons::PATIENTS);

    $user = $this->users_repository->create(Uuid::generate()->string,$request->get('name'),$request->get('ap_patern'),
    $request->get('ap_matern'),$request->get('email'),Hash::make($request->get('password')),
    $request->get('validation').substr($request->get('name'), 0, 3) .substr($request->get('email'), 0, 3).'2020',
    $person->id);

    $patients = $this->patients_respository->create(Uuid::generate()->string,$request->get('living_place'),$request->get('blood_type'),
    $request->get('disability'),$request->get('ethnic_group'),$request->get('religion'),$request->get('socioeconomic_level'),
    $request->get('age'),$request->get('hospitals_id'),$person->id);

    $token = JWTAuth::fromUser($user);

    $datas['subject'] = 'HealthWell';
    $datas['for'] = $user['email'];
    Mail::send('mail.mail',['user' =>$user], function($msj) use($datas){
    $msj->from("healthwellapp@gmail.com","HealthWell");
    $msj->subject($datas['subject']);
    $msj->to($datas['for']);
    });
    return response()->json(compact('user','token','person','patients'),201);
    }



public function updated(Request $request, $uuid){
    $user2 = User::where('uuid','=',$uuid)->first();

$patients = $this->patients_respository->update($user2->persons->patients->uuid,$request->get('living_place'),$request->get('blood_type'),
$request->get('disability'),$request->get('ethnic_group'),$request->get('religion'),$request->get('socioeconomic_level'),
$request->get('age'),$request->get('hospitals_id'));

$person = $this->persons_repository->update($user2->persons->uuid,$request->get('name'),$request->get('ap_patern'),
$request->get('ap_matern'),$request->get('curp'),$request->get('domicile'),$request->get('cell_phone'),
$request->get('telefone'),$request->get('photo'));


$user = $this->users_repository->update($user2->uuid,$request->get('name'),$request->get('ap_patern'),
$request->get('ap_matern'),$request->get('email'));

return response()->json(compact('user','person','patients'),201);
}

public function delete($uuid){
$user = User::where('uuid','=',$uuid)->first();
$user->persons->patients->delete();
$user->persons->delete();
$user->delete();


return response()->json('Datos eliminados');
}
    public function list(){
        return response()->json($this->patients_respository->list());
    }


    public function editar($uuid)
    {

        $user = User::where('uuid','=',$uuid)->first();
        $person = Persons::where('uuid','=',$user->persons->uuid)->first();
        $patients = Patients::where('uuid','=',$user->persons->patients->uuid)->first();


        $masvar = [
        'id'=>$person['id'],
        'uuid'=>$user['uuid'],
        'name'=>$person['name'],
        'ap_patern'=>$person['ap_patern'],
        'ap_matern'=>$person['ap_matern'],
        'curp'=>$person['curp'],
        'domicile'=>$person['domicile'],
        'cell_phone'=>$person['cell_phone'],
        'telefone'=>$person['telefone'],
        'photo'=>$person['photo'],
        'roles_id'=>$person['roles_id'],
        'email'=>$user['email'],
        'name'=>$user['name'],
        'persons_id'=>$user['persons_id'],
        'living_place'=>$patients['living_place'],
        'blood_type'=>$patients['blood_type'],
        'disability'=>$patients['disability'],
        'ethnic_group'=>$patients['ethnic_group'],
        'religion'=>$patients['religion'],
        'socioeconomic_level'=>$patients['socioeconomic_level'],
        'age'=>$patients['age'],
        'hospitals_id'=>$patients['hospitals_id'],
        ];

        return response()->json($masvar);
    }

    public function upload(Request $request){
        $image = $request->file('file0');



        $validator = Validator::make($request->all(), [

        'file0' => 'mimes:jpeg,jpg,png|required',

        ]);
        if($validator->fails()){
        return response()->json($validator->errors()->toJson(), 400);
        }


        $image_name = time().$image->getClientOriginalName();
        \Storage::disk('images')->put($image_name, \File::get($image));
        $data = array(
            'code' => 200,
            'imagen' => $image_name,
            'status' => 'success'
        );

            return response()->json($data, $data['code']);
        }

}
