<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\RolesRepository;
use App\Models\Roles;
use DB;

class RolesController extends Controller
{
    protected $roles_respository;

    public function __construct(RolesRepository $repository){
        $this->roles_respository = $repository;
    }

    public function list(){
        return response()->json($this->roles_respository->list());
    }

}
