<?php

namespace App\Http\Controllers;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Repositories\HospitalsRepository;
use App\Models\Hospitals;
use DB;
use Uuid;
use File;
use Illuminate\Support\Facades\Validator;

class HospitalsController extends Controller
{
    protected $hospitals_respository;

    public function __construct(HospitalsRepository $repository){
        $this->hospitals_respository = $repository;
    }

    public function create(Request $request){

        $uuid = Uuid::generate()->string;
        $name = $request->input('name');
        $direction =$request->input('direction');
        $photo =$request->input('photo');
        return response()->json($this->hospitals_respository->create($uuid,$name, $direction,$photo));
    }

    /////////////////////////////////////////////
    public function updated(Request $request, $uuid)
    {

        $name = $request->input('name');
        $direction =$request->input('direction');
        $photo =$request->input('photo');

       return response()->json($this->hospitals_respository->updated($uuid, $name, $direction,$photo));
    }


    public function delete($uuid){
        return response()->json($this->hospitals_respository->delete($uuid));
    }
    public function list(){
        return response()->json($this->hospitals_respository->list());
    }


   /* public function search(Request $request){
       $search = $request->input('search');
    return response()->json($this->hospitals_respository->search($search));
    }*/


    public function editar($uuid)
    {
        return response()->json($this->hospitals_respository->find($uuid));
    }

    public function return_image($name)
    {
    $imagen = \Storage::disk('Hospital')->exists($name);

    if($imagen){
    $file = \Storage::disk('Hospital')->get($name);
    return new Response ($file,201);
    }else{
     return response()->json('No existe la imagen');

    }

    }

    public function upload(Request $request){
    $image = $request->file('file0');



    $validator = Validator::make($request->all(), [

        'file0' => 'mimes:jpeg,jpg,png|required',

    ]);
    if($validator->fails()){
    return response()->json($validator->errors()->toJson(), 400);
    }


    $image_name = time().$image->getClientOriginalName();
    \Storage::disk('Hospital')->put($image_name, \File::get($image));
    $data = array(
        'code' => 200,
        'imagen' => $image_name,
        'status' => 'success'
    );

        return response()->json($data, $data['code']);
    }
    }

