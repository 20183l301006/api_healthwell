<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\InquiriesRepository;
use App\Models\inquiries;
use DB;
use Uuid;

class inquiriesController extends Controller
{
    protected $inquieries_respository;

    public function __construct(InquiriesRepository $repository){
        $this->inquieries_respository = $repository;
    }

    public function create(Request $request){

        $uuid = Uuid::generate()->string;
        $num_inquirie = $request->input('num_inquirie');
        $tratamiento = inquiries::DIRECTION_PATIENTS;
        $patients_id =$request->input('patients_id');
        $doctors_id =$request->input('doctors_id');
        return response()->json($this->inquieries_respository->create($uuid,$num_inquirie, $tratamiento,$patients_id,$doctors_id));
    }

    /////////////////////////////////////////////
    public function updated(Request $request, $uuid)
    {

        $num_inquirie = $request->input('num_inquirie');
        $tratamiento =$request->input('tratamiento');
        $patients_id =$request->input('patients_id');
        $doctors_id =$request->input('doctors_id');
       return response()->json($this->inquieries_respository->updated($uuid,$num_inquirie, $tratamiento,$patients_id,$doctors_id));
    }


    public function delete($uuid){
        return response()->json($this->inquieries_respository->delete($uuid));
    }
    public function list(){
        return response()->json($this->inquieries_respository->list());
    }


   /* public function search(Request $request){
       $search = $request->input('search');
    return response()->json($this->hospitals_respository->search($search));
    }*/


    public function editar($uuid)
    {
        return response()->json($this->inquieries_respository->find($uuid));
    }


}
