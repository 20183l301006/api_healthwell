<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\HospitalsController;
use App\Http\Controllers\inquiriesController;
use App\Http\Controllers\PatientsController;
use App\Http\Controllers\RecipeController;
use App\Http\Controllers\RolesController;
use App\Http\Controllers\ShuleApointController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/


Route::post('hospitals',[HospitalsController::class,'create']);
Route::put('hospitals/{uuid}',[HospitalsController::class,'updated']);
Route::delete('hospitals/{uuid}',[HospitalsController::class,'delete']);
Route::get('hospitals',[HospitalsController::class,'list']);
Route::get('hospitals/{uuid}',[HospitalsController::class,'editar']);
Route::post('hospitals/upload',[HospitalsController::class,'upload']);
Route::get('hospitals/upload/{name}',[HospitalsController::class,'return_image']);
//Route::get('count','HospitalsController@empresas');
Route::post('admin', [HospitalsController::class,'register']);

Route::get('roles',[RolesController::class,'list']);

Route::post('register', [UserController::class,'register']);
Route::get('users', [UserController::class,'list']);
Route::get('validar',[UserController::class,'validar']);
Route::post('login', [UserController::class,'authenticate']);
Route::get('users/{uuid}',[UserController::class,'editar']);
Route::put('users/{uuid}',[UserController::class,'updated']);
Route::delete('users/{uuid}',[UserController::class,'@delete']);
Route::post('upload',[UserController::class,'upload']);
Route::get('upload/{name}',[UserController::class,'return_image']);


Route::post('patients', [PatientsController::class,'create']);
Route::get('patients', [PatientsController::class,'list']);
//Route::post('login', 'UserController@authenticate');
Route::get('patients/{uuid}',[PatientsController::class,'editar']);
Route::put('patients/{uuid}',[PatientsController::class,'updated']);
Route::delete('patients/{uuid}',[PatientsController::class,'delete']);
Route::post('patients/upload',[PatientsController::class,'upload']);
Route::get('patients/upload/{name}',[PatientsController::class,'return_image']);

Route::post('shedule',[ShuleApointController::class,'create']);
Route::put('shedule/{uuid}',[ShuleApointController::class,'updated']);
Route::delete('shedule/{uuid}',[ShuleApointController::class,'delete']);
Route::get('shedule',[ShuleApointController::class,'list']);
Route::get('shedule/{uuid}',[ShuleApointController::class,'editar']);


Route::post('recipe',[RecipeController::class,'create']);
Route::put('recipe/{uuid}',[RecipeController::class,'updated']);
Route::delete('recipe/{uuid}',[RecipeController::class,'delete']);
Route::get('recipe',[RecipeController::class,'list']);
Route::get('recipe/{uuid}',[RecipeController::class,'editar']);

Route::group(['middleware' => ['api.auth']], function() {
});
