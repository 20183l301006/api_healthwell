<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Domicile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mongodb')->create('domicile', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid');
            $table->varchar('type'); //tipo
            $table->varchar('street'); //calle
            $table->integer('number_ext'); //numero exterior
            $table->integer('number_int'); //numero interior
            $table->varchar('state'); //estado
            $table->varchar('municipality'); //municipio
            $table->varchar('location'); //localidad
            $table->varchar('colony'); //colonia
            $table->varchar('postalCode'); //codigo postal
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
