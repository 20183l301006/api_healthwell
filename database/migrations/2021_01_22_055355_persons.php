<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Persons extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mongodb')->create('persons', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid');
            $table->string('name'); //apellido paterno
            $table->string('ap_patern'); //apellido paterno
            $table->string('ap_matern');   //apellido materno
            $table->string('curp')->unique(); //curp
            $table->string('domicile'); //domicilio
            $table->string('cell_phone'); //celular
            $table->string('telefone'); //telefono
            $table->string('photo'); //foto
            $table->integer('roles_id')->unsigned();
            $table->foreign('roles_id')->references('id')->on('roles');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
