<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Hospitals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mongodb')->create('hospitals', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid');
            $table->varchar('name'); //nombre
            $table->varchar('direction');   //direccion
            $table->varchar('photo');   //direccion
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
